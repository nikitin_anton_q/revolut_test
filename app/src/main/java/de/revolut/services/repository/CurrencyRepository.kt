package de.revolut.services.repository

import de.revolut.extensions.inject
import de.revolut.models.CurrencyModel
import de.revolut.services.network.RequestProvider
import java.net.UnknownHostException

class CurrencyRepository {

    /**
     * inejction of RequestProvider
     */
    val network by inject<RequestProvider>()

    suspend fun load(baseCurrency: String): CurrencyModel? {
        return try {
            network.currency(baseCurrency).currencyModel
        } catch (e: UnknownHostException) {
            null
        } catch (e: Exception) {
            e.printStackTrace()
            null
        } ?: return null
    }
}