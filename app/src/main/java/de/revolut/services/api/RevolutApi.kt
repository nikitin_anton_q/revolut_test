package de.revolut.services.api

import de.revolut.models.CurrencyModel
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("latest")
    suspend fun getCurrency(@Query("base") base: String?): CurrencyModel
}