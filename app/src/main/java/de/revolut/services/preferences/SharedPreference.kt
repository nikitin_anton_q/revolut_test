package de.revolut.services.preferences

import android.content.Context
import android.content.SharedPreferences
import de.revolut.BuildConfig

/**
 * class of Shared Preferences
 *
 * @property context context
 */
class SharedPreference (val context: Context) {
    /**
     * pref name constant
     */
    companion object{
        private const val PREFS_NAME = BuildConfig.APPLICATION_ID
    }

    /**
     * shared pref variable
     */
    private val sharedPref: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    /**
     * checking the given key is already in shared pref
     *
     * @param key control key
     * @return is in sharedPref or not
     */
    fun contains(key: String): Boolean {
        return sharedPref.contains(key)
    }

    /**
     * getting all keys in sharedprefs
     *
     * @return list of keys which saved in sharedPref
     */
    fun getAll(): List<String> {
        return sharedPref.all.keys.toList()
    }

    /**
     * saving the given key and value to shared pref
     *
     * @param key Key for save
     * @param text value wanted to be saved
     */
    fun save(key: String, text: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(key, text)
        editor.apply()
    }

    /**
     * saving the given key and value to shared pref
     *
     * @param key Key for save
     * @param value value wanted to be saved
     */
    fun save(key: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putInt(key, value)
        editor.apply()
    }


    /**
     * saving the given key and value to shared pref
     *
     * @param key Key for save
     * @param value value wanted to be saved
     */
    fun save(key: String, value: Double) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putLong(key, java.lang.Double.doubleToRawLongBits(value))
        editor.apply()
    }


    /**
     * saving the given key and value to shared pref
     *
     * @param key Key for save
     * @param status value wanted to be saved
     */
    fun save(key: String, status: Boolean) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putBoolean(key, status)
        editor.apply()
    }

    /**
     * saving the given key and value to shared pref
     *
     * @param key Key for save
     * @param value value wanted to be saved
     */
    fun save(key: String, value: Long) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putLong(key, value)
        editor.apply()
    }

    /**
     * getter for string from the sharedPref
     *
     * @param key Key for get
     * @return saved value
     */
    fun getValueString(key: String): String? {
        return sharedPref.getString(key, null)
    }

    /**
     * getter for string from the sharedPref
     *
     * @param key Key for get
     * @return saved value
     */
    fun getValueInt(key: String): Int {
        return sharedPref.getInt(key, 0)
    }

    /**
     * getter for long from the sharedPref
     *
     * @param key Key for get
     * @return saved value
     */
    fun getValueLong(key: String): Long {
        return sharedPref.getLong(key, 0)
    }


    /**
     * getter for string from the sharedPref
     *
     * @param key Key for get
     * @return saved value
     */
    fun getValueBoolean(key: String, defaultValue: Boolean): Boolean {
        return sharedPref.getBoolean(key, defaultValue)
    }

    /**
     * clear method of sharedPref
     *
     */
    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.apply()
    }

    /**
     * removing the given key value from the sharePref
     *
     * @param key Key for delete
     */
    fun removeValue(key: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.remove(key)
        editor.apply()
    }
}
