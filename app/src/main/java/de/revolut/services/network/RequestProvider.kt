package de.revolut.services.network

import androidx.annotation.WorkerThread
import de.revolut.extensions.inject
import de.revolut.models.ResponseBody
import de.revolut.services.api.RevolutApi

class RequestProvider {
    /**
     * revolutApi injection
     */
    private val revolutApi by inject<RevolutApi>()

    /**
     * getting currency list
     *
     * @return list of pref data under ResponseBody
     */
    @WorkerThread
    suspend fun currency(baseCurrency: String?): ResponseBody = ResponseBody(revolutApi.getCurrency(baseCurrency))
}