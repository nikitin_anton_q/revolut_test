package de.revolut.koin

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.revolut.R
import de.revolut.extensions.resString
import de.revolut.models.CurrencyModel
import de.revolut.services.api.RevolutApi
import de.revolut.services.interceptors.ContentTypeInterceptor
import de.revolut.services.network.RequestProvider
import net.kibotu.logger.Logger
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


var gson = GsonBuilder()
    .setLenient()
    .create()

val networkModule = module {
    single { RequestProvider() }
    single { Dispatcher() }
    single { createOkHttpClient(get()).build() }
    single { createWebService<RevolutApi>(get(), R.string.base_url.resString) }
}

private fun createOkHttpClient(dispatcher: Dispatcher): OkHttpClient.Builder = OkHttpClient.Builder()
    .retryOnConnectionFailure(true)
    .dispatcher(dispatcher)
    .addInterceptor(ContentTypeInterceptor())
    .addInterceptor(createHttpLoggingInterceptor { true })

private inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T = Retrofit.Builder()
    .baseUrl(url)
    .client(okHttpClient)
    .addConverterFactory(GsonConverterFactory.create(gson))
    .build()
    .create(T::class.java)

fun createHttpLoggingInterceptor(enableLogging: () -> Boolean): HttpLoggingInterceptor = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        Logger.v(message)
    }
}).apply {
    level = if (enableLogging())
        HttpLoggingInterceptor.Level.BODY
    else
        HttpLoggingInterceptor.Level.NONE
}