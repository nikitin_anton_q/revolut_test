package de.revolut.koin

import de.revolut.services.preferences.SharedPreference
import org.koin.dsl.module

val configurationModule = module {
    single { SharedPreference(get()) }
}