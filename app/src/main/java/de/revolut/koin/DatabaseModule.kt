package de.revolut.koin

import de.revolut.services.repository.CurrencyRepository
import org.koin.dsl.module

val localDataSourceModule = module {
    single { CurrencyRepository() }
}