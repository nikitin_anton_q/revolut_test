package de.revolut.extensions

import net.kibotu.ContextHelper

val Int.resString: String
    get() = ContextHelper.getApplication()!!.resources!!.getString(this)