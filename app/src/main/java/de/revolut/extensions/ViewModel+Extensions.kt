package de.revolut.extensions

import androidx.annotation.MainThread
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import net.kibotu.ContextHelper

/**
 *  lazy viewmodel initializer
 *
 * @param VM
 */
@MainThread
inline fun <reified VM : ViewModel> viewModel() = lazy { ViewModelProviders.of(ContextHelper.getAppCompatActivity()!!).get(VM::class.java) }