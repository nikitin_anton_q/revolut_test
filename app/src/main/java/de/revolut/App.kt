package de.revolut

import android.app.Application
import de.revolut.koin.configurationModule
import de.revolut.koin.localDataSourceModule
import de.revolut.koin.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    configurationModule,
                    networkModule,
                    localDataSourceModule
                )
            )
        }
    }
}