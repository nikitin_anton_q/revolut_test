package de.revolut.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.revolut.extensions.inject
import de.revolut.models.CurrencyModel
import de.revolut.services.preferences.SharedPreference
import de.revolut.services.repository.CurrencyRepository
import kotlinx.coroutines.*
import java.util.*

class AppViewModel : ViewModel() {

    val currencyRepository by inject<CurrencyRepository>()
    val sharedPreference by inject<SharedPreference>()

    /**
     * This is the job for all coroutines started by this ViewModel.
     * Cancelling this will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()
    val currencyModel = MutableLiveData<CurrencyModel>()
    lateinit var timer: Timer

    /**
     * coroutines uiScope
     */
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    fun loadCurrency() {
        timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                uiScope.launch {
                    withContext(Dispatchers.IO) {
                        currencyModel.postValue(currencyRepository.load(sharedPreference.getValueString("baseCurrency") ?: "EUR"))
                    }
                }
            }
        }, 1, 1000)
    }
}