package de.revolut.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import de.revolut.R
import de.revolut.extensions.inject
import de.revolut.extensions.viewModel
import de.revolut.models.Rates
import de.revolut.services.preferences.SharedPreference
import de.revolut.ui.main.adapter.RatesAdapter
import de.revolut.ui.main.adapter.interfaces.RateInterface
import de.revolut.viewModels.AppViewModel
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment(), RateInterface {

    private val appViewModel by viewModel<AppViewModel>()
    lateinit var adapter: RatesAdapter
    private val sharedPreference by inject<SharedPreference>()
    private var stopUpdate: Boolean = false
    private var currentPosition: Int = -1

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        appViewModel.loadCurrency()
        setupViews()
        setObservers()
    }

    /**
     * setup observers
     *
     */
    private fun setObservers() {
        appViewModel.currencyModel.observe(viewLifecycleOwner, Observer {
            if (!stopUpdate) {
                val items = arrayListOf<Rates>()
                items.add(0, Rates(it.baseCurrency, 100.0))
                it.rates?.forEach { rate ->
                    items.add(Rates(rate.key, rate.value))
                }
                if (currentPosition == -1) {
                    adapter.addRates(items)
                } else {
                    adapter.updateRates(items, currentPosition)
                }
            }
        })
    }

    /**
     * setup views
     *
     */
    private fun setupViews() {
        adapter = RatesAdapter(mutableListOf(), this)
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        recycler_view.adapter = adapter
        (recycler_view.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
    }

    override fun onDestroy() {
        super.onDestroy()
        appViewModel.currencyModel.removeObservers(this)
    }

    override fun onBaseValueChanged(position: Int) {
        currentPosition = position
        recycler_view.post {
            when {
                position > 0 && position < adapter.itemCount -> {
                    adapter.notifyItemRangeChanged(0, position)
                    adapter.notifyItemRangeChanged(position, adapter.itemCount - position)
                }
                position == 0 -> {
                    adapter.notifyItemRangeChanged(1, adapter.itemCount)
                }
                position == adapter.itemCount -> {
                    adapter.notifyItemRangeChanged(0, adapter.itemCount - 2)
                }
            }
        }
    }

    override fun onItemClick(rate: Rates) {
        currentPosition = -1
        recycler_view.smoothScrollToPosition(0)
        sharedPreference.save("baseCurrency", rate.currency ?: "EUR")
        appViewModel.loadCurrency()
    }

    override fun stopUpdate(stopUpdate: Boolean) {
        this.stopUpdate = stopUpdate
    }

    override fun onFocusChange(focus: Boolean, position: Int) {
        currentPosition = if (focus)
            position
        else
            -1
    }
}