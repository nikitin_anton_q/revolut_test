package de.revolut.ui.main.adapter.interfaces

import de.revolut.models.Rates

interface RateInterface {
    fun onBaseValueChanged(position: Int)
    fun onItemClick(rate: Rates)
    fun stopUpdate(stopUpdate: Boolean)
    fun onFocusChange(focus: Boolean, position: Int)
}