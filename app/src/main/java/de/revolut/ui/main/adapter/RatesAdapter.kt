package de.revolut.ui.main.adapter

import android.icu.util.Currency
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.exozet.android.core.extensions.onClick
import com.exozet.android.core.ui.nullobjects.onTextChanged
import de.revolut.R
import de.revolut.models.Rates
import de.revolut.ui.main.adapter.interfaces.RateInterface
import kotlinx.android.synthetic.main.item_rate.view.*
import java.text.DecimalFormat

class RatesAdapter(private var rates: List<Rates>, private val rateInterface: RateInterface) :
    RecyclerView.Adapter<RatesAdapter.RateViewHolder>() {

    private var baseCurrencyValue: Double = 1.0
    private val decimalFormat = DecimalFormat("#,###.##")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RateViewHolder(inflater, parent)
    }

    fun addRates(rates: List<Rates>) {
        this.rates = rates
        notifyDataSetChanged()
    }

    fun updateRates(rates: List<Rates>, position: Int) {
        this.rates = rates
        when (position) {
            in 1 until (itemCount - 1) -> {
                notifyItemRangeChanged(0, position )
                notifyItemRangeChanged(position + 1, itemCount - position)
            }
            0 -> {
                notifyItemRangeChanged(1, itemCount)
            }
            itemCount -> {
                notifyItemRangeChanged(0, itemCount - 2)
            }
        }
    }

    override fun getItemCount(): Int {
        return rates.size
    }

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        val rate = rates[position]
        holder.bind(rate, position)
    }

    inner class RateViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_rate, parent, false)) {

        fun bind(rate: Rates, position: Int) = with(itemView) {
            rate_value.isEnabled = position == 0
            rate_code.text = rate.currency
            rate_name.text = Currency.getInstance(rate.currency).displayName
            rate_value.setText("${rate.value}")

            Glide.with(itemView.context)
                .load("https://raw.githubusercontent.com/hjnilsson/country-flags/master/png1000px/${rate.currency?.substring(0, rate.currency.length.minus(1))?.toLowerCase()}.png")
                .circleCrop()
                .into(rate_image)

            rate_value.setOnFocusChangeListener { _, focused ->
                if (focused)
                    rate_value.setSelection(rate_value.length())
                rateInterface.onFocusChange(focused, position)
            }

            rate_value.setOnClickListener {
                rateInterface.stopUpdate(true)
            }

            onClick {
                rateInterface.onItemClick(rate)
                baseCurrencyValue = 1.0
            }

            if (!rate_value.isFocused) {
                rate_value.setText(decimalFormat.format(rate.value?.times(baseCurrencyValue)))
            }

            if (baseCurrencyValue == 0.0)
                rate_value.setText("")

            rate_value.onTextChanged { s, _, _, _ ->
                if (rate_value.isFocused && !s.isNullOrBlank()) {
                    baseCurrencyValue = s.toString().replace(",", "").toDouble()
                    rateInterface.onBaseValueChanged(position)
                    Handler().postDelayed({
                        rateInterface.stopUpdate(false)
                    }, 1000)
                } else if (rate_value.isFocused && s.isNullOrBlank()) {
                    baseCurrencyValue = 0.0
                    rateInterface.onBaseValueChanged(position)
                }
            }
        }
    }
}