package de.revolut.models

data class ResponseBody(
    var currencyModel: CurrencyModel? = null
)