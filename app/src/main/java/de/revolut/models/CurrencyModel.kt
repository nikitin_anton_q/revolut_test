package de.revolut.models

data class CurrencyModel(
    var baseCurrency: String? = null,
    var rates: LinkedHashMap<String, Double>? = null
)