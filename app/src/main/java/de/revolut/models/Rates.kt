package de.revolut.models

data class Rates(
    val currency: String? = null,
    var value: Double? = null
)